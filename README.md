## jhead - clone of head(1)

jhead(1) is a clone of head(1) for systems that does not
have a full featured head(1).  But it may have a couple
of options not found in head(1).

If head(1) exists on your system, you should use that
instead.  It will be much faster.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/jhead) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/jhead.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/jhead.gmi (mirror)

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
